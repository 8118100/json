# tcgfox

A dark theme for Firefox (Windows), by tcgmoo.

WARNING: Involves advanced stuff like userChrome and userContent, and changing prefs, which could end up breaking something. Proceed at your own risk!

Compatible with letterboxing!

## Prerequisites

1. You need to install [Sidebery](https://addons.mozilla.org/en-US/firefox/addon/sidebery/)
2. You need to make sure that these prefs have the correct setting in about:config:

- browser.tabs.drawInTitlebar -> false
- toolkit.legacyUserProfileCustomizations.stylesheets -> true
- ui.popup.disable_autohide -> false

## Browser Installation

1. Go to about:profiles
2. Find the profile you want to customise with this theme
3. Click on the "Open folder" button in the "Root Directory" section of that profile
4. Make a folder called "chrome" (without the quotation marks), if there isn't one already
5. Place only userChrome.css and the userChrome folder in that directory if you don't want to change any of the about pages
6. Place userContent.css and the userContent folder in that directory if you want to change the about pages as well
7. You can open and edit userChrome.css and userContent.css to change the accent color to any color you wish to

## Sidebery Installation

1. Go to about:addons
2. Go to Options for Sidebery (3-dot menu -> Options)
3. Go to Styles Editor
4. Paste everything in Sidebery.txt into the text box on the right (under Sidebar)
5. You can further customise Sidebery by selecting colors in the Styles Editor, or in the other menus, or by editing the text you just pasted. You can also import sidebery-data.json to copy my look exactly.

## Screenshots

![Screenshot 1](/assets/screenshots/Screenshot1.png)
![Screenshot 2](/assets/screenshots/Screenshot2.png)
![Screenshot 3](/assets/screenshots/Screenshot3.png)
![Screenshot 4](/assets/screenshots/Screenshot4.png)

---

I hope you enjoy my Firefox theme!
